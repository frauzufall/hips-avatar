package javafx;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.Timer;
import java.util.TimerTask;

public class Ghost extends Application {

    private final int sideMove = 100;
    private int sideMoveCurrent = 50;
    private boolean moveLeft = true;
    private double posX, posY;
    private Robot robot;

    @Override
    public void start(Stage primaryStage) throws AWTException {
        robot = new Robot();

        primaryStage.initStyle(StageStyle.UTILITY);
        primaryStage.setOpacity(0.);

        Stage stage = new Stage();
        stage.initOwner(primaryStage);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setAlwaysOnTop(true);
        ImageView imageView = new ImageView(createGhost());
        VBox box = new VBox();
        box.getChildren().add(imageView);
        stage.setScene(createScene(box));
        primaryStage.show();
        stage.show();
        center(stage);
        whiggle(stage);
        avoidMouse(stage);
    }

    private Scene createScene(VBox box) {
        final Scene scene = new Scene(box);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        scene.setFill(null);
        return scene;
    }

    private void center(Stage stage) {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        double posX = (primScreenBounds.getWidth() - stage.getWidth()) / 2;
        double posY = (primScreenBounds.getHeight() - stage.getHeight()) / 2;
        updateTarget(posX, posY);
        stage.setX(this.posX);
        stage.setY(this.posY);
    }

    private void updateTarget(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    private Image createGhost() {
        return new Image(getClass().getResource("/ghost.gif").toString());
    }

    private void whiggle(Stage stage) {
        new Timer("move").scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if(sideMoveCurrent < 0) moveLeft = false;
                if(sideMoveCurrent > sideMove) moveLeft = true;
                sideMoveCurrent += moveLeft? -1 : 1;
                stage.setX(stage.getX() + (moveLeft? -1 : 1));
                double val = (System.nanoTime() % 200) / 200. * 2*Math.PI;
                stage.setY(stage.getY() + (int)(Math.sin(val) * 2));
            }
        }, 0, 200);
    }

    private void avoidMouse(Stage stage) {
//        new Timer("avoid-mouse").scheduleAtFixedRate(new TimerTask() {
//            public void run() {
//                int mouseX = robot.getMouseX();
//                int mouseY = robot.getMouseY();
//                int stageX = (int) stage.getX();
//                int stageY = (int) stage.getY();
//                int difX = stageX - mouseX;
//                int difY = stageY - mouseY;
//                double abs = Math.sqrt(difX*difX + difY*difY);
//                if(abs < 100) {
//                    stage.setX(stageX + difX* (1./abs));
//                    stage.setY(stageY + difY* (1./abs));
//                }
//            }
//        }, 0, 100);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
