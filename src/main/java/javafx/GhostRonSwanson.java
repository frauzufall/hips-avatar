package javafx;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GhostRonSwanson extends Application {

    private final int sideMove = 100;
    private int sideMoveCurrent = 50;
    private boolean moveLeft = true;

    @Override
    public void start(Stage stage) {
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setAlwaysOnTop(true);
        Text chatMessage = new Text("BUG");
        ImageView imageView = new ImageView(createGhost());
        attachTalking(chatMessage, imageView);
        VBox box = new VBox();
        attachChatBubble(box, chatMessage);
        box.getChildren().add(imageView);
        stage.setScene(createScene(box));
        stage.show();
        center(stage);
        whiggle(stage);
    }

    private Scene createScene(VBox box) {
        final Scene scene = new Scene(box);
        scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        scene.setFill(null);
        return scene;
    }

    private void center(Stage stage) {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);
    }

    private Image createGhost() {
        return new Image(getClass().getResource("/ghost.gif").toString());
    }

    private void attachTalking(Text chatMessage, ImageView imageView) {
        HttpGet getRequest = new HttpGet(
                "https://ron-swanson-quotes.herokuapp.com/v2/quotes");
        DefaultHttpClient httpClient = new DefaultHttpClient();
        getRequest.addHeader("accept", "application/json");
        chatMessage.setText(nextMessage(httpClient, getRequest));
        imageView.setOnMouseClicked(event -> {
            chatMessage.setText(nextMessage(httpClient, getRequest));
        });
    }

    private void attachChatBubble(VBox box, Text chatMessage) {
        VBox textBox = new VBox();
        VBox styleBox = new VBox();
        chatMessage.setWrappingWidth(170);
        Font font = Font.font("Arial", FontPosture.REGULAR, 13);
        chatMessage.setFont(font);
        chatMessage.setLineSpacing(1.25);
        textBox.setBackground(new Background(new BackgroundFill(new Color(1, 1, 1, 1), null, null)));
        textBox.getChildren().add(chatMessage);
        textBox.getStyleClass().add("chat-top");
        styleBox.getStyleClass().add("chat-bottom");
        box.getChildren().add(textBox);
        box.getChildren().add(styleBox);
    }

    private void whiggle(Stage stage) {
        new Timer("move").scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if(sideMoveCurrent < 0) moveLeft = false;
                if(sideMoveCurrent > sideMove) moveLeft = true;
                sideMoveCurrent += moveLeft? -1 : 1;
                stage.setX(stage.getX() + (moveLeft? -1 : 1));
                stage.setY(stage.getY() + (int)(Math.sin(System.nanoTime()/10000.) * 10));
            }
        }, 0, 100);
    }

    private String nextMessage(DefaultHttpClient httpClient, HttpGet getRequest) {
        HttpResponse response = null;
        try {
            response = httpClient.execute(getRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }

        ObjectMapper mapper = new ObjectMapper();
        String json = "{\"name\":\"mkyong\", \"age\":\"37\"}";

        try {

            // convert JSON string to Map
            List<String> list = mapper.readValue(response.getEntity().getContent(), List.class);
            return list.get(0);

        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
