package swing;

import com.jhlabs.image.GaussianFilter;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static java.awt.GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSPARENT;
import static java.awt.GraphicsDevice.WindowTranslucency.TRANSLUCENT;

/**
 * From JavaMagazine May/June 2012
 * @author josh
 */
public class Ghost {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Determine what the GraphicsDevice can support.
        GraphicsEnvironment ge =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        final boolean isTranslucencySupported =
                gd.isWindowTranslucencySupported(TRANSLUCENT);

        //If shaped windows aren't supported, exit.
        if (!gd.isWindowTranslucencySupported(PERPIXEL_TRANSPARENT)) {
            System.err.println("Shaped windows are not supported");
            System.exit(0);
        }

        //If translucent windows aren't supported,
        //create an opaque window.
        if (!isTranslucencySupported) {
            System.out.println(
                    "Translucency is not supported, creating an opaque window");
        }
        //switch to the right thread
        SwingUtilities.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }

            JFrame frame = new JFrame();
            frame.setUndecorated(true);
            frame.setBackground(new Color(1.0f,1.0f,1.0f,0.0f));
            frame.setAlwaysOnTop(true);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            try {
                frame.add(new Pixel(frame));
            } catch (IOException e) {
                e.printStackTrace();
            }
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        });
    }

    private static void listWindows() {
        // call xwininfo -root -children for linux
        // explore https://stackoverflow.com/questions/15491288/get-a-list-of-all-open-windows-from-the-command-line for windows
    }

    private static class Pixel extends JPanel {

        private final Image avatar[] = new Image[9];
        private final Composite composite;
        private final Frame frame;
        private int count = 0;
        private int sideMove = 100;
        private int sideMoveCurrent = 50;
        private boolean moveLeft = true;
        private boolean countPaint;

        public Pixel(Frame frame) throws IOException {
            this.frame = frame;
            for (int i = 1; i < 9; i++) {
                avatar[i-1] = ImageIO.read(getClass().getResource("/ghost/ghost" + i + ".png"));
            }
            Graphics2D gbi = (Graphics2D) avatar[0].getGraphics();
            composite = gbi.getComposite();
            setOpaque(false);

            new Timer("whiggle").scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    count = (count + 1) % 8;
                    SwingUtilities.invokeLater(() -> repaint());
                }
            }, 0, 300);
//            initMove(frame);
        }

        private void initMove(Frame frame) {
            new Timer("move").scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    if(sideMoveCurrent < 0) moveLeft = false;
                    if(sideMoveCurrent > sideMove) moveLeft = true;
                    sideMoveCurrent += moveLeft? -1 : 1;
                    frame.setLocation(frame.getLocation().x + (moveLeft? -1 : 1), frame.getLocation().y + (int)(Math.sin(System.nanoTime()/10000.) * 10));
                }
            }, 0, 100);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(Math.max(10, avatar[0].getWidth(this)), Math.max(10, avatar[0].getHeight(this)));
        }

        public void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            Image img = avatar[count];
            Graphics2D g = (Graphics2D) graphics.create();
//            g.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR, 1.0f));
//            g.setColor(new Color(255, 255, 255, 255));
//            g.fill(new Rectangle2D.Double(0, 0, img.getWidth(this), img.getHeight(this)));
//            g.clearRect(0, 0, getWidth(), getHeight());
            g.setComposite(AlphaComposite.SrcOver.derive(0.0f));
            g.setColor(getBackground());
            g.fillRect(0, 0, getWidth(), getHeight());
//            g.setColor(new Color(0, 0, 0, 0));
//            g.fillRect(0, 0, getWidth(),getHeight());
//            g.setComposite(composite);
            g.setComposite(AlphaComposite.SrcOver.derive(1.0f));
            g.drawImage(img,0, 0, this);
            // set the color of line drawn to blue
//            g.setColor(Color.blue);
//            g.setStroke(new BasicStroke(1));
            // draw the polygon using drawPolygon function
//            g.drawRect(0, 0, 42, 42);
//            g.drawString("My Killer App", 200, 100);
            graphics.dispose();
            countPaint = !countPaint;
        }


        public static void applyQualityProperties(Graphics2D g2) {
            g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        }

        public BufferedImage generateShadow(BufferedImage imgSource, int size, Color color, float alpha) {

            int imgWidth = imgSource.getWidth() + (size * 2);
            int imgHeight = imgSource.getHeight() + (size * 2);

            BufferedImage imgMask = createCompatibleImage(imgWidth, imgHeight);
            Graphics2D g2 = imgMask.createGraphics();
            applyQualityProperties(g2);

            int x = Math.round((imgWidth - imgSource.getWidth()) / 2f);
            int y = Math.round((imgHeight - imgSource.getHeight()) / 2f);
//            g2.drawImage(imgSource, x, y, null);
            g2.drawImage(imgSource, 0, 0, null);
            g2.dispose();

            // ---- Blur here ---

            BufferedImage imgShadow = generateBlur(imgMask, size, color, alpha);

            return imgShadow;

        }


        public BufferedImage createCompatibleImage(int width, int height, int transparency) {

            BufferedImage image = getGraphicsConfiguration().createCompatibleImage(width, height, transparency);
            image.coerceData(true);
            return image;

        }

        public BufferedImage createCompatibleImage(int width, int height) {

            return createCompatibleImage(width, height, Transparency.TRANSLUCENT);

        }


        public BufferedImage generateBlur(BufferedImage imgSource, int size, Color color, float alpha) {

            GaussianFilter filter = new GaussianFilter(size);

            int imgWidth = imgSource.getWidth();
            int imgHeight = imgSource.getHeight();

            BufferedImage imgBlur = createCompatibleImage(imgWidth, imgHeight);
            Graphics2D g2d = imgBlur.createGraphics();
            applyQualityProperties(g2d);

            g2d.drawImage(imgSource, 0, 0, null);
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_IN, alpha));
            g2d.setColor(color);

            g2d.fillRect(0, 0, imgSource.getWidth(), imgSource.getHeight());
            g2d.dispose();

            imgBlur = filter.filter(imgBlur, null);

            return imgBlur;

        }
    }
}
